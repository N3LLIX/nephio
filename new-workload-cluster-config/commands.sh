kubectl --kubeconfig ../.kube/mgmt-config apply -f gitea-edge-repo-edgetest.yaml -f cluster-edgetest.yaml -f cluster-context-edgetest.yaml -f cluster-scale-profile-edgetest.yaml 
kpt --kubeconfig ../.kube/edgetest.config live init nephio-configsync --force
kpt --kubeconfig ../.kube/edgetest.config live apply nephio-configsync 
kubectl --kubeconfig ../.kube/edgetest.config apply -f gitea-secret.yaml -f gitea-root-sync-edgetest.yaml
